﻿using System;


namespace zadanie6._1
{
    internal class Punkt
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public Punkt(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Punkt() : this(0, 0) { }
        public Punkt(Punkt oryg) : this(oryg.X, oryg.Y) { }
        public static bool operator ==(Punkt p1, Punkt p2) => (p1.X == p2.X && p1.Y == p2.Y);
        public static bool operator !=(Punkt p1, Punkt p2) => (p1.X != p2.X || p1.Y != p2.Y);
        public void Przesuń(int dx, int dy)
        {
            X += dx;
            Y += dy;
        }
        public override string ToString()
        {
            return "(" + X.ToString() + ", " + Y.ToString() + ")";
        }
    }
    class Linia
    {
        public Punkt P1 { get; private set; }
        public Punkt P2 { get; private set; }
        public Linia(Punkt p1, Punkt p2)
        {
            P1 = p1;
            P2 = p2;
        }
        public Linia() : this(new Punkt(0, 0), new Punkt(1, 1)) { }
        public Linia(Linia oryg) : this(oryg.P1, oryg.P2) { }
        public void Przesuń(int dx, int dy)
        {
            P1 = new Punkt(P1);
            P2 = new Punkt(P2);
            P1.Przesuń(dx, dy);
            P2.Przesuń(dx, dy);
        }
        public override string ToString()
        {
            return P1 + ", " + P2;
        }
        public bool PołączonaZ(Linia l2) => P1 == l2.P1 || P1 == l2.P2 || P2 == l2.P1 || P2 == l2.P2;
        public double Długość()
        {
            return Math.Sqrt(Math.Pow(P1.X - P2.X, 2) + Math.Pow(P1.Y - P2.Y, 2));
        }
    }
    class Trójkąt
    {
        public Linia L1 { get; private set; }
        public Linia L2 { get; private set; }
        public Linia L3 { get; private set; }
        public Trójkąt(Linia l1, Linia l2, Linia l3)
        {
            if (!l1.PołączonaZ(l2) || !l2.PołączonaZ(l3) || !l3.PołączonaZ(l1)) {
                Console.WriteLine("Z tych lini można zbudować trójkąt");
            }
            if (l1.Długość() + l2.Długość() > l3.Długość() && l1.Długość() + l3.Długość() > l2.Długość() && l2.Długość() + l3.Długość() > l1.Długość())
            {
                L1 = l1;
                L2 = l2;
                L3 = l3;
            }
            else
            {
                Console.WriteLine("Z tych lini nie można zbudować trójkąta");
            }

        }

        public Trójkąt(Trójkąt oryg) : this(oryg.L1, oryg.L2, oryg.L3) { }
        public void Przesuń(int dx, int dy)
        {
            L1 = new Linia(L1);
            L2 = new Linia(L2);
            L3 = new Linia(L3);
            L1.Przesuń(dx, dy);
            L2.Przesuń(dx, dy);
            L3.Przesuń(dx, dy);
        }
        public override string ToString()
        {
            return L1 + ", " + L2 + ", " + L3;
        }
    }
    class Czworokąt
    {
        public Linia L1 { get; private set; }
        public Linia L2 { get; private set; }
        public Linia L3 { get; private set; }
        public Linia L4 { get; private set; }
        public Czworokąt(Linia l1, Linia l2, Linia l3, Linia l4)
        {
            if (!l1.PołączonaZ(l2) || !l2.PołączonaZ(l3) || !l3.PołączonaZ(l4) || !l4.PołączonaZ(l1))
            {
                Console.WriteLine("Linie są ze sobą połączone");
            }
            L1 = l1;
            L2 = l2;
            L3 = l3;
            L4 = l4;
        }
        public Czworokąt(Czworokąt oryg) : this(oryg.L1, oryg.L2, oryg.L3, oryg.L4) { }
        public void Przesuń(int dx, int dy)
        {
            L1 = new Linia(L1);
            L2 = new Linia(L2);
            L3 = new Linia(L3);
            L4 = new Linia(L4);
            L1.Przesuń(dx, dy);
            L2.Przesuń(dx, dy);
            L3.Przesuń(dx, dy);
            L4.Przesuń(dx, dy);
        }
        public override string ToString()
        {
            return L1 + ", " + L2 + ", " + L3 + ", " + L4;
        }
    }
    class Obraz
    {
        public object[] ar;
        int count;
        public Obraz()
        {
            ar = new object[3];
            count = 0;
        }
        public void Dodaj(object figura)
        {
            if (count + 1 >= 3 && count + 1 > ar.Length - 1)
            {
                object[] copy = new object[ar.Length];
                ar.CopyTo(copy, 0);
                ar = new object[ar.Length * 2];
            }
            ar[count] = figura;
            count++;
        }
        public void Usuń(int index)
        {
            ar[index] = null;
            count--;
            if (count + 1 > 3 && count + 1 < ar.Length - 1)
            {
                object[] copy = new object[ar.Length];
                ar.CopyTo(copy, 0);
                ar = new object[ar.Length / 2];
            }
        }
    }

    class Program
    {
        static void Main()
        {

            Punkt p1 = new Punkt(2, 7);
            Punkt p2 = new Punkt(6, 13);
            Punkt p3 = new Punkt(2, 5);
            Punkt p4 = new Punkt(3, 9);
            Punkt p5 = new Punkt(10, 15);
            Linia l1 = new Linia(p1, p2);
            Linia l2 = new Linia(p1, p3);
            Linia l3 = new Linia(p2, p3);
            Linia l4 = new Linia(p1, p4);
            Linia l5 = new Linia(p1, p5);
            Trójkąt t1 = new Trójkąt(l1, l2, l3);
            Trójkąt t2 = new Trójkąt(l1, l2, l4);
            Trójkąt t3 = new Trójkąt(l3, l4, l1);
            Trójkąt t4 = new Trójkąt(l3, l4, l2);
            Czworokąt cz1 = new Czworokąt(l1, l2, l3, l4);
            Czworokąt cz2 = new Czworokąt(l1, l2, l3, l5);
            Obraz o1 = new Obraz();
        }
    }
}

